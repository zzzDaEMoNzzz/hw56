import React from 'react';
import './Tries.css';

const Tries = props => {
  return (
    <p className="Tries">
      Tries: <span>{props.count}</span>
    </p>
  );
};

export default Tries;
