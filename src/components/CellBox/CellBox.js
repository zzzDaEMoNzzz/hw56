import React from 'react';
import './CellBox.css';

import Cell from '../Cell/Cell';

const CellBox = props => {
  const cellBoxWidth = {
    width: props.cellsInRow * (props.cellSize + 2)
  };

  const cellBoxInit = () => {
    let Cells = [];
    for (let i = 0; i < props.activeCells.length; i++) {
      let hasItem = false;
      if (i === props.cellWithItem) {
        hasItem = true;
      }

      Cells.push(
        <Cell
          size={props.cellSize}
          hasItem={hasItem}
          isActive={props.activeCells[i]}
          deactivateCell={props.deactivateCell(i)}
          increaseTries={props.increaseTries}
          isGameActive={props.isGameActive}
          stopGame={props.stopGame}
          key={i}
        />
      );
    }

    return Cells;
  };

  return (
    <div className="CellBox" style={cellBoxWidth}>
      {cellBoxInit()}
    </div>
  );
};

export default CellBox;
