import React from 'react';

const Reset = props => {
  return (
    <div>
      <button onClick={props.onClick}>Reset</button>
    </div>
  );
};

export default Reset;
