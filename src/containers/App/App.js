import React, { Component } from 'react';
import './App.css';

import CellBox from '../../components/CellBox/CellBox';
import Tries from '../../components/Tries/Tries';
import Reset from '../../components/Reset/Reset';

const cellsInRow = 6;

const getCellsArray = () => {
  let cellsArray = [];

  for (let i = 0; i < Math.pow(cellsInRow, 2); i++) {
    cellsArray.push(true);
  }

  return cellsArray;
};

class App extends Component {
  state = {
    isGameActive: true,
    activeCells: getCellsArray(),
    cellWithItem: Math.floor(Math.random() * Math.pow(cellsInRow, 2)),
    tries: 0
  };

  deactivateCell (index) {
    return () => {
      let cells = [...this.state.activeCells];
      cells[index] = false;

      this.setState({activeCells: cells});
    };
  }

  increaseTries() {
    const tries = this.state.tries + 1;
    this.setState({tries});
  }

  restartGame() {
    this.setState({
      isGameActive: true,
      activeCells: getCellsArray(),
      cellWithItem: Math.floor(Math.random() * Math.pow(cellsInRow, 2)),
      tries: 0
    });
  }

  stopGame() {
    this.setState({
      isGameActive: false
    });
  }

  render() {
    return (
      <div className="App">
        <CellBox
          cellsInRow={cellsInRow}
          cellSize={28}
          activeCells={this.state.activeCells}
          cellWithItem={this.state.cellWithItem}
          deactivateCell={this.deactivateCell.bind(this)}
          increaseTries={this.increaseTries.bind(this)}
          isGameActive={this.state.isGameActive}
          stopGame={this.stopGame.bind(this)}
        />
        <Tries count={this.state.tries}/>
        <Reset onClick={this.restartGame.bind(this)}/>
      </div>
    );
  }
}

export default App;
